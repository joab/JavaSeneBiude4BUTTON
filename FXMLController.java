package application;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

public class FXMLController implements Initializable{
	
	@FXML
	private Label label1;
	
	@FXML
	private void handleButtoonActio1 (ActionEvent event) {
		System.out.println("Butao 1!");
		label1.setText("Tiago ");
		
	}
	
	@FXML
	private Label label2;
	
	@FXML
	private void handleButtoonActio2 (ActionEvent event) {
		System.out.println("Butao 2!");
		label2.setText("Brenda");
		
	}
	
	@FXML
	private Label label3;
	
	@FXML
	private void handleButtoonActio3 (ActionEvent event) {
		System.out.println("Butao 3!");
		label3.setText("Batista");
		
	}
	
	@FXML
	private Label label4;
	
	@FXML
	private void handleButtoonActio4 (ActionEvent event) {
		System.out.println("Butao 4!");
		label4.setText("Valdileia");
		
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}


}
